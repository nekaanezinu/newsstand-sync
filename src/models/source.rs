use sqlx::types::time::PrimitiveDateTime;

pub struct Source {
    pub id: i32,
    pub name: String,
    pub url: String,
    pub user_id: i32,
    pub external_id: String,
    pub created_at: Option<PrimitiveDateTime>,
    pub updated_at: Option<PrimitiveDateTime>,
}
