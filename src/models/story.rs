use sqlx::types::time::PrimitiveDateTime;

pub struct Story {
    pub id: i32,
    pub score: i32,
    pub title: String,
    pub url: String,
    pub favorited_at: PrimitiveDateTime,
    pub published_at: PrimitiveDateTime,
    pub external_id: Option<String>,
    pub created_at: Option<PrimitiveDateTime>,
    pub updated_at: Option<PrimitiveDateTime>,
    pub source_id: i32,
    pub user_id: i32,
}
