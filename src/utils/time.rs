use std::time::Duration;

use prost_types::Timestamp;
use sqlx::types::time::{OffsetDateTime, PrimitiveDateTime};

pub fn timestamp_to_primitive_datetime(timestamp: &Timestamp) -> PrimitiveDateTime {
    let offset_dt = OffsetDateTime::UNIX_EPOCH
        + Duration::new(timestamp.seconds as u64, timestamp.nanos as u32);
    PrimitiveDateTime::new(offset_dt.date(), offset_dt.time())
}

pub fn primitive_datetime_to_timestamp(datetime: PrimitiveDateTime) -> Timestamp {
    let offset_dt = datetime.assume_utc();
    Timestamp {
        seconds: offset_dt.unix_timestamp(),
        nanos: offset_dt.nanosecond() as i32,
    }
}

pub fn current_primitive_datetime() -> PrimitiveDateTime {
    let now_utc = OffsetDateTime::now_utc();
    // who the fuck does not include a current time method ffs
    PrimitiveDateTime::new(now_utc.date(), now_utc.time())
}
