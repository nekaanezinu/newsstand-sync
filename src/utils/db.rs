use sqlx::{migrate::MigrateError, postgres::PgPoolOptions, PgPool};
use std::{collections::HashMap, env};
use thiserror::Error;

lazy_static! {
    pub static ref DB_POOL: PgPool = {
        PgPoolOptions::new()
            .max_connections(5)
            .connect_lazy(connection_string().as_str())
            .expect("Failed to create pool")
    };
}

#[derive(Error, Debug)]
pub enum DbInitError {
    #[error("database connection error")]
    ConnectionError(#[from] sqlx::Error),

    #[error("migration error")]
    MigrationError(#[from] MigrateError),
}

pub struct Db {}

impl Db {
    pub async fn init() -> Result<(), DbInitError> {
        migrate(&*DB_POOL).await?;
        Ok(())
    }

    // just check if db works
    pub async fn _query(&self) -> Result<(), sqlx::Error> {
        let row: (i64,) = sqlx::query_as("SELECT $1")
            .bind(150_i64)
            .fetch_one(&*DB_POOL)
            .await?;

        assert_eq!(row.0, 150);

        Ok(())
    }
}

async fn migrate(pool: &PgPool) -> Result<(), MigrateError> {
    sqlx::migrate!("./migrations").run(pool).await
}

fn connection_string() -> String {
    let db_keys = vec!["DB_USER", "DB_PASSWORD", "DB_HOST", "DB_NAME"];

    let mut connection_values: HashMap<String, String> = HashMap::new();

    for key in db_keys {
        match env::var(&key) {
            Ok(value) => connection_values.insert(key.to_string(), value),
            Err(e) => {
                panic!("Error: {}, on key {}", e.to_string(), key)
            }
        };
    }

    format!(
        "postgres://{}:{}@{}/{}",
        connection_values.get("DB_USER").unwrap(),
        connection_values.get("DB_PASSWORD").unwrap(),
        connection_values.get("DB_HOST").unwrap(),
        connection_values.get("DB_NAME").unwrap()
    )
}
