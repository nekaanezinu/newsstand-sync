use tonic::{Request, Response, Status};

use crate::{
    auth::Auth,
    models::source::Source,
    repositories::{
        queries::{source, sources},
        sources::SourcesRepository,
    },
    services::sources::proto_sources::{
        sources_crud_server::SourcesCrud, CreateSourceRequest, GetSourceRequest, SourceResponse,
    },
};

use self::proto_sources::{GetSourcesRequest, SourcesResponse};

use super::mutations::sources::{Args, CreateInput, SourceMutations};

pub mod proto_sources {
    tonic::include_proto!("sources");
}

pub struct SourcesService {
    repository: SourcesRepository,
}

impl SourcesService {
    pub fn new() -> Self {
        Self {
            repository: SourcesRepository::new(),
        }
    }
}

#[tonic::async_trait]
impl SourcesCrud for SourcesService {
    async fn get_sources(
        &self,
        request: Request<GetSourcesRequest>,
    ) -> Result<Response<SourcesResponse>, Status> {
        let user = Auth::authenticate(request.metadata()).await?;

        match self.repository.all(sources::Scope::User(user)).await {
            Ok(sources) => Ok(sources_to_reponse(&sources)),
            Err(_err) => Err(Status::aborted("something very wrong")),
        }
    }

    async fn get_source(
        &self,
        request: Request<GetSourceRequest>,
    ) -> Result<Response<SourceResponse>, Status> {
        let user = Auth::authenticate(request.metadata()).await?;

        let message = request.get_ref();
        if message.id == 0 {
            return Err(Status::invalid_argument("id must be present"));
        }

        match self
            .repository
            .find(source::Scope::User(user, message.id as i32))
            .await
        {
            Ok(source) => Ok(source_to_response(&source)),
            Err(_err) => Err(Status::not_found("not found")),
        }
    }

    async fn create_source(
        &self,
        request: Request<CreateSourceRequest>,
    ) -> Result<Response<SourceResponse>, Status> {
        let user = Auth::authenticate(request.metadata()).await?;

        let mutation =
            SourceMutations::new(Args::Create(CreateInput::from_request(&request)), user);

        match mutation.run().await {
            Ok(source) => Ok(source_to_response(&source)),
            Err(err) => {
                println!("{:#?}", err);

                Err(Status::aborted("lmao"))
            }
        }
    }
}

fn source_to_response(source: &Source) -> Response<SourceResponse> {
    Response::new(source_to_source_response(source))
}

fn sources_to_reponse(sources: &Vec<Source>) -> Response<SourcesResponse> {
    let source_responses: Vec<SourceResponse> = sources
        .iter()
        .map(|s| source_to_source_response(s))
        .collect();

    Response::new(SourcesResponse {
        sources: source_responses,
    })
}

fn source_to_source_response(source: &Source) -> SourceResponse {
    SourceResponse {
        id: source.id.clone() as i64,
        name: source.name.clone(),
        url: source.url.clone(),
        external_id: source.external_id.clone(),
    }
}
