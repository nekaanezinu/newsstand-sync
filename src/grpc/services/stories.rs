use crate::{
    repositories::queries::{stories, story},
    services::{
        mutations::stories::{Args, CreateInput},
        stories::proto_stories::{
            stories_crud_server::StoriesCrud, CreateStoryRequest, GetStoriesRequest,
            GetStoryRequest, StoriesResponse, StoryResponse,
        },
    },
    utils::time,
};
use tonic::{Request, Response, Status};

use crate::{auth::Auth, models::story::Story, repositories::stories::StoriesRepository};

use super::mutations::stories::StoryMutations;

pub mod proto_stories {
    tonic::include_proto!("stories");
}

pub struct StoriesService {
    repository: StoriesRepository,
}

impl StoriesService {
    pub fn new() -> Self {
        Self {
            repository: StoriesRepository::new(),
        }
    }
}

#[tonic::async_trait]
impl StoriesCrud for StoriesService {
    async fn get_stories(
        &self,
        request: Request<GetStoriesRequest>,
    ) -> Result<Response<StoriesResponse>, Status> {
        let user = Auth::authenticate(request.metadata()).await?;

        match self
            .repository
            .all_paginated(stories::Scope::User(user), request.get_ref().cursor, 30)
            .await
        {
            Ok(paged_result) => Ok(stories_to_reponse(&paged_result.list, paged_result.cursor)),
            Err(_err) => Err(Status::aborted("something very wrong")),
        }
    }

    async fn get_story(
        &self,
        request: Request<GetStoryRequest>,
    ) -> Result<Response<StoryResponse>, Status> {
        let user = Auth::authenticate(request.metadata()).await?;

        let message = request.get_ref();
        if message.id == 0 {
            return Err(Status::invalid_argument("id must be present"));
        }

        match self
            .repository
            .find(story::Scope::User(user, message.id as i32))
            .await
        {
            Ok(story) => Ok(story_to_response(&story)),
            Err(_err) => Err(Status::not_found("not found")),
        }
    }

    async fn create_story(
        &self,
        request: Request<CreateStoryRequest>,
    ) -> Result<Response<StoryResponse>, Status> {
        let user = Auth::authenticate(request.metadata()).await?;

        let mutation = StoryMutations::new(Args::Create(CreateInput::from_request(&request)), user);

        match mutation.run().await {
            Ok(story) => Ok(story_to_response(&story)),
            Err(err) => {
                println!("{:#?}", err);

                Err(Status::aborted("lmao"))
            }
        }
    }
}

fn story_to_response(story: &Story) -> Response<StoryResponse> {
    Response::new(story_to_story_response(story))
}

fn stories_to_reponse(stories: &Vec<Story>, next_cursor: i32) -> Response<StoriesResponse> {
    let story_responses: Vec<StoryResponse> =
        stories.iter().map(|s| story_to_story_response(s)).collect();

    Response::new(StoriesResponse {
        next_cursor,
        stories: story_responses,
    })
}

fn story_to_story_response(story: &Story) -> StoryResponse {
    StoryResponse {
        id: story.id.clone(),
        title: story.title.clone(),
        url: story.url.clone(),
        external_id: story.external_id.clone().unwrap(),
        score: story.score.clone(),
        published_at: Some(time::primitive_datetime_to_timestamp(story.published_at)),
    }
}
