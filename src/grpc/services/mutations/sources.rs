use tonic::Request;

use crate::{
    grpc::services::sources::proto_sources::CreateSourceRequest, models::{source::Source, user::User},
    repositories::sources::SourcesRepository,
};

pub struct CreateInput {
    pub name: String,
    pub url: String,
    pub external_id: String,
}

impl CreateInput {
    pub fn from_request(request: &Request<CreateSourceRequest>) -> Self {
        let message = request.get_ref().clone();

        Self {
            name: message.name.clone(),
            url: message.url.clone(),
            external_id: message.external_id.clone(),
        }
    }
}

pub enum Args {
    Create(CreateInput),
}

pub struct SourceMutations {
    args: Args,
    user: User
}

impl SourceMutations {
    pub fn new(args: Args, user: User) -> Self {
        SourceMutations { args, user }
    }

    pub async fn run(&self) -> Result<Source, sqlx::Error> {
        match &self.args {
            Args::Create(input) => self.create(input).await,
        }
    }

    async fn create(&self, input: &CreateInput) -> Result<Source, sqlx::Error> {
        let repo = SourcesRepository::new();
        repo.create(input, self.user.clone()).await
    }
}
