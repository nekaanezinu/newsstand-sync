use crate::{
    grpc::services::stories::proto_stories::CreateStoryRequest,
    models::{story::Story, user::User},
    repositories::stories::StoriesRepository,
};
use prost_types::Timestamp;
use tonic::Request;

pub struct CreateInput {
    pub title: String,
    pub url: String,
    pub external_id: String,
    pub score: i32,
    pub source_id: i32,
    pub published_at: Timestamp,
}

impl CreateInput {
    pub fn from_request(request: &Request<CreateStoryRequest>) -> Self {
        let message = request.get_ref().clone();

        Self {
            title: message.title.clone(),
            url: message.url.clone(),
            external_id: message.external_id.clone(),
            score: message.score.clone(),
            // TODO: scope this properly, use db index?
            source_id: message.source_id.clone(),
            published_at: message.published_at.unwrap().clone(),
        }
    }
}

pub enum Args {
    Create(CreateInput),
}

pub struct StoryMutations {
    args: Args,
    user: User,
}

impl StoryMutations {
    pub fn new(args: Args, user: User) -> Self {
        StoryMutations { args, user }
    }

    pub async fn run(&self) -> Result<Story, sqlx::Error> {
        match &self.args {
            Args::Create(input) => self.create(input).await,
        }
    }

    async fn create(&self, input: &CreateInput) -> Result<Story, sqlx::Error> {
        let repo = StoriesRepository::new();
        repo.create(input, self.user.clone()).await
    }
}
