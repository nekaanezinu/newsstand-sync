use tonic::Request;

use crate::{
    grpc::services::users::users::CreateUserRequest, models::user::User,
    repositories::users::UsersRepository,
};

pub struct CreateInput {
    pub name: String,
}

impl CreateInput {
    pub fn from_request(request: &Request<CreateUserRequest>) -> Self {
        let message = request.get_ref().clone();

        Self {
            name: message.name.clone(),
        }
    }
}

pub enum Args {
    Create(CreateInput),
}

pub struct UserMutations {
    args: Args,
}

impl UserMutations {
    pub fn new(args: Args) -> Self {
        UserMutations { args }
    }

    pub async fn run(&self) -> Result<User, sqlx::Error> {
        match &self.args {
            Args::Create(input) => self.create(input).await,
        }
    }

    async fn create(&self, input: &CreateInput) -> Result<User, sqlx::Error> {
        let repo = UsersRepository::new();
        repo.create(input).await
    }
}
