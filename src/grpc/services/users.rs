use tonic::{Request, Response, Status};

use super::mutations::users::{Args, CreateInput, UserMutations};
use crate::auth::Auth;
use crate::models::user::User;
use crate::repositories::users::UsersRepository;
use crate::services::users::users::{
    users_crud_server::UsersCrud, CreateUserRequest, GetUserRequest, UserResponse,
};

pub mod users {
    tonic::include_proto!("users");
}

pub struct UsersService {}

impl UsersService {
    pub fn new() -> Self {
        Self {}
    }
}

#[tonic::async_trait]
impl UsersCrud for UsersService {
    async fn get_user(
        &self,
        request: Request<GetUserRequest>,
    ) -> Result<Response<UserResponse>, Status> {
        let _user = Auth::authenticate(request.metadata()).await?;

        let message = request.get_ref();

        if message.id == 0 {
            return Err(Status::invalid_argument("id must be present"));
        };

        let repo = UsersRepository::new();

        match repo.find(message.id).await {
            Ok(user) => Ok(user_to_response(&user)),
            Err(_err) => Err(Status::not_found("not found")),
        }
    }

    async fn create_user(
        &self,
        request: Request<CreateUserRequest>,
    ) -> Result<Response<UserResponse>, Status> {
        let _user = Auth::authenticate(request.metadata()).await?;

        let mutation = UserMutations::new(Args::Create(CreateInput::from_request(&request)));

        match mutation.run().await {
            Ok(user) => Ok(user_to_response(&user)),
            Err(_err) => Err(Status::aborted("lmao")), // TODO
        }
    }
}

fn user_to_response(user: &User) -> Response<UserResponse> {
    Response::new(UserResponse {
        id: user.id.clone(),
        name: user.name.clone(),
        secret: user.secret.clone(),
    })
}
