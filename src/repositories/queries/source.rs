use tonic::async_trait;

use crate::{
    models::{source::Source, user::User},
    utils::db::DB_POOL,
};

use super::ScopedQuery;

#[derive(Clone, Debug)]
pub enum Scope {
    All(i32), // id
    User(User, i32),
}

#[derive(Clone, Debug)]
pub struct Query {
    scope: Scope,
}

impl Query {
    pub fn new(scope: Scope) -> Query {
        Query { scope }
    }

    pub fn simple(&self) -> SourceQuery<Simple> {
        SourceQuery {
            structure: Simple { scope: self.scope.clone() },
        }
    }
}

#[derive(Clone, Debug)]
pub struct Simple {
    scope: Scope,
}

#[derive(Clone, Debug)]
pub struct SourceQuery<T> {
    structure: T,
}

impl SourceQuery<Simple> {
    async fn all(&self, id: i32) -> Result<Source, sqlx::Error> {
        sqlx::query_as!(
            Source,
            "SELECT * FROM sources WHERE id = $1 ORDER BY id;",
            id
        )
        .fetch_one(&*DB_POOL)
        .await
    }

    async fn for_user(&self, user: User, id: i32) -> Result<Source, sqlx::Error> {
        sqlx::query_as!(
            Source,
            "SELECT * FROM sources WHERE user_id = $1 AND id = $2 ORDER BY id;",
            user.id as i32,
            id
        )
        .fetch_one(&*DB_POOL)
        .await
    }
}

#[async_trait]
impl ScopedQuery<Source> for SourceQuery<Simple> {
    async fn query(&self) -> Result<Source, sqlx::Error> {
        match self.structure.scope.clone() {
            Scope::All(id) => self.all(id).await,
            Scope::User(user, id) => self.for_user(user, id).await,
        }
    }
}
