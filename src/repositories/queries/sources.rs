use tonic::async_trait;

use crate::{
    models::{source::Source, user::User},
    utils::db::DB_POOL,
};

use super::ScopedQuery;

#[derive(Clone, Debug)]
pub enum Scope {
    All,
    User(User),
}

#[derive(Clone, Debug)]
pub struct Query {
    scope: Scope,
}

impl Query {
    pub fn new(scope: Scope) -> Query {
        Query { scope }
    }

    pub fn simple(&self) -> SourcesQuery<Simple> {
        SourcesQuery {
            structure: Simple { scope: self.scope.clone() },
        }
    }
}

#[derive(Clone, Debug)]
pub struct SourcesQuery<T> {
    structure: T,
}

#[derive(Clone, Debug)]
pub struct Simple {
    scope: Scope,
}

impl SourcesQuery<Simple> {
    async fn all(&self) -> Result<Vec<Source>, sqlx::Error> {
        sqlx::query_as!(Source, "SELECT * FROM sources ORDER BY id;")
            .fetch_all(&*DB_POOL)
            .await
    }

    async fn for_user(&self, user: User) -> Result<Vec<Source>, sqlx::Error> {
        sqlx::query_as!(
            Source,
            "SELECT * FROM sources WHERE user_id = $1 ORDER BY id;",
            user.id as i32
        )
        .fetch_all(&*DB_POOL)
        .await
    }
}

#[async_trait]
impl ScopedQuery<Vec<Source>> for SourcesQuery<Simple> {
    async fn query(&self) -> Result<Vec<Source>, sqlx::Error> {
        match self.structure.scope.clone() {
            Scope::All => self.all().await,
            Scope::User(user) => self.for_user(user).await,
        }
    }
}
