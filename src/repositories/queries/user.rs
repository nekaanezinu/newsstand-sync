use tonic::async_trait;

use crate::{models::user::User, utils::db::DB_POOL};

use super::ScopedQuery;

#[derive(Clone, Debug)]
pub enum Scope {
    All(i32),
    Auth(String),
}

#[derive(Clone, Debug)]
pub struct Query {
    scope: Scope,
}

impl Query {
    pub fn new(scope: Scope) -> Query {
        Query { scope }
    }

    pub fn simple(&self) -> UserQuery<Simple> {
        UserQuery {
            structure: Simple { scope: self.scope.clone() },
        }
    }
}

#[derive(Clone, Debug)]
pub struct Simple {
    scope: Scope,
}

pub struct UserQuery<T> {
    structure: T,
}

impl UserQuery<Simple> {
    async fn all(&self, id: i32) -> Result<User, sqlx::Error> {
        sqlx::query_as!(User, "SELECT id, name, secret FROM users WHERE id = $1", id)
            .fetch_one(&*DB_POOL)
            .await
    }

    async fn for_auth(&self, secret: String) -> Result<User, sqlx::Error> {
        sqlx::query_as!(
            User,
            // TODO: add index
            "SELECT id, name, secret FROM users WHERE secret = $1",
            secret
        )
        .fetch_one(&*DB_POOL)
        .await
    }
}

#[async_trait]
impl ScopedQuery<User> for UserQuery<Simple> {
    async fn query(&self) -> Result<User, sqlx::Error> {
        match self.structure.clone().scope {
            Scope::All(id) => self.all(id).await,
            Scope::Auth(secret) => self.for_auth(secret).await,
        }
    }
}
