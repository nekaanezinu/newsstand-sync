use tonic::async_trait;

use crate::{
    models::{story::Story, user::User},
    utils::db::DB_POOL,
};

use super::ScopedQuery;

#[derive(Clone, Debug)]
pub enum Scope {
    All(i32), // id
    User(User, i32),
}

#[derive(Clone, Debug)]
pub struct Query {
    scope: Scope,
}

impl Query {
    pub fn new(scope: Scope) -> Query {
        Query { scope }
    }

    pub fn simple(&self) -> StoryQuery<Simple> {
        StoryQuery {
            structure: Simple { scope: self.scope.clone() },
        }
    }
}

#[derive(Clone, Debug)]
pub struct Simple {
    scope: Scope,
}

#[derive(Clone, Debug)]
pub struct StoryQuery<T> {
    structure: T,
}

impl StoryQuery<Simple> {
    async fn all(&self, id: i32) -> Result<Story, sqlx::Error> {
        sqlx::query_as!(
            Story,
            "SELECT * FROM stories WHERE id = $1 ORDER BY id;",
            id
        )
        .fetch_one(&*DB_POOL)
        .await
    }

    async fn for_user(&self, user: User, id: i32) -> Result<Story, sqlx::Error> {
        sqlx::query_as!(
            Story,
            "SELECT * FROM stories WHERE user_id = $1 AND id = $2 ORDER BY id;",
            user.id as i32,
            id
        )
        .fetch_one(&*DB_POOL)
        .await
    }
}

#[async_trait]
impl ScopedQuery<Story> for StoryQuery<Simple> {
    async fn query(&self) -> Result<Story, sqlx::Error> {
        match self.structure.clone().scope {
            Scope::All(id) => self.all(id).await,
            Scope::User(user, id) => self.for_user(user, id).await,
        }
    }
}
