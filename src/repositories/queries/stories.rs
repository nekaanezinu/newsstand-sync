use tonic::async_trait;

use crate::{
    models::{story::Story, user::User},
    utils::db::DB_POOL,
};

use super::{PaginatedList, ScopedQuery};

#[derive(Clone, Debug)]
pub enum Scope {
    All,
    User(User),
}

#[derive(Clone, Debug)]
pub struct Query {
    scope: Scope,
}

impl Query {
    pub fn new(scope: Scope) -> Query {
        Query { scope }
    }

    pub fn simple(&self) -> StoriesQuery<Simple> {
        StoriesQuery {
            structure: Simple { scope: self.scope.clone() },
        }
    }

    pub fn paginated(&self, cursor: i32, limit: i32) -> StoriesQuery<Paginated> {
        StoriesQuery {
            structure: Paginated {
                scope: self.scope.clone(),
                cursor,
                limit,
            },
        }
    }
}

#[derive(Clone, Debug)]
pub struct Paginated {
    pub scope: Scope,
    pub cursor: i32,
    pub limit: i32,
}

#[derive(Clone, Debug)]
pub struct Simple {
    pub scope: Scope,
}

#[derive(Clone, Debug)]
pub struct StoriesQuery<T> {
    structure: T,
}

impl StoriesQuery<Simple> {
    async fn all(&self) -> Result<Vec<Story>, sqlx::Error> {
        sqlx::query_as!(Story, "SELECT * FROM stories ORDER BY id;")
            .fetch_all(&*DB_POOL)
            .await
    }

    async fn for_user(&self, user: User) -> Result<Vec<Story>, sqlx::Error> {
        sqlx::query_as!(
            Story,
            "SELECT * FROM stories WHERE user_id = $1 ORDER BY id;",
            user.id as i32
        )
        .fetch_all(&*DB_POOL)
        .await
    }
}

impl StoriesQuery<Paginated> {
    async fn all(&self) -> Result<PaginatedList<Vec<Story>>, sqlx::Error> {
        // TODO: maybe leave these as lean as possible and build results elsewhere?
        let list = sqlx::query_as!(
            Story,
            "SELECT * FROM stories WHERE id > $1 ORDER BY id LIMIT $2",
            self.structure.cursor as i32,
            self.structure.limit as i32
        )
        .fetch_all(&*DB_POOL)
        .await?;

        Ok(self.build_paginated_list(list))
    }

    async fn for_user(&self, user: User) -> Result<PaginatedList<Vec<Story>>, sqlx::Error> {
        let list = sqlx::query_as!(
            Story,
            "SELECT * FROM stories WHERE user_id = $1 AND id > $2 ORDER BY id LIMIT $3",
            user.id as i32,
            self.structure.cursor as i32,
            self.structure.limit as i32
        )
        .fetch_all(&*DB_POOL)
        .await?;

        Ok(self.build_paginated_list(list))
    }

    fn build_paginated_list(&self, list: Vec<Story>) -> PaginatedList<Vec<Story>> {
        let cursor = list
            .last()
            .map_or_else(|| 0, |item| item.id.clone());
        PaginatedList { list, cursor }
    }
}

#[async_trait]
impl ScopedQuery<Vec<Story>> for StoriesQuery<Simple> {
    async fn query(&self) -> Result<Vec<Story>, sqlx::Error> {
        match self.structure.scope.clone() {
            Scope::All => self.all().await,
            Scope::User(user) => self.for_user(user).await,
        }
    }
}

#[async_trait]
impl ScopedQuery<PaginatedList<Vec<Story>>> for StoriesQuery<Paginated> {
    async fn query(&self) -> Result<PaginatedList<Vec<Story>>, sqlx::Error> {
        match self.structure.scope.clone() {
            Scope::All => self.all().await,
            Scope::User(user) => self.for_user(user).await,
        }
    }
}
