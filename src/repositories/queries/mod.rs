use tonic::async_trait;

pub mod source;
pub mod sources;
pub mod stories;
pub mod story;
pub mod user;

#[derive(Clone, Debug)]
pub struct PaginatedList<T> {
    pub list: T,
    pub cursor: i32,
}

#[async_trait]
pub trait ScopedQuery<T> {
    async fn query(&self) -> Result<T, sqlx::Error>;
}
