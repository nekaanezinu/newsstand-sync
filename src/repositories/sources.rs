use crate::{
    grpc::services::mutations::sources::CreateInput,
    models::{source::Source, user::User},
};

use crate::utils::db::DB_POOL;

use super::queries::{source, sources, ScopedQuery};
pub struct SourcesRepository {}

impl SourcesRepository {
    pub fn new() -> Self {
        SourcesRepository {}
    }

    pub async fn all(&self, scope: sources::Scope) -> Result<Vec<Source>, sqlx::Error> {
        sources::Query::new(scope).simple().query().await
    }

    pub async fn find(&self, scope: source::Scope) -> Result<Source, sqlx::Error> {
        source::Query::new(scope).simple().query().await
    }

    pub async fn create(&self, source: &CreateInput, user: User) -> Result<Source, sqlx::Error> {
        let row = sqlx::query!(
            "INSERT INTO sources (name, url, user_id, external_id) VALUES ($1, $2, $3, $4) RETURNING ID;",
            source.name.as_str(),
            source.url.as_str(),
            user.id as i32,
            source.external_id.as_str(),
        )
        .fetch_one(&*DB_POOL)
        .await?;

        self.find(source::Scope::User(user, row.id)).await
    }
}
