use uuid::Uuid;

use crate::{
    grpc::services::mutations::users::CreateInput, models::user::User, utils::db::DB_POOL,
};

use super::queries::{user, ScopedQuery};

pub struct UsersRepository {}

impl UsersRepository {
    pub fn new() -> Self {
        UsersRepository {}
    }

    pub async fn find(&self, id: i32) -> Result<User, sqlx::Error> {
        user::Query::new(user::Scope::All(id))
            .simple()
            .query()
            .await
    }

    pub async fn find_by_secret(&self, secret: String) -> Result<User, sqlx::Error> {
        user::Query::new(user::Scope::Auth(secret))
            .simple()
            .query()
            .await
    }

    pub async fn create(&self, user: &CreateInput) -> Result<User, sqlx::Error> {
        let row = sqlx::query!(
            "INSERT INTO users (name, secret) VALUES ($1, $2) RETURNING ID;",
            user.name.as_str(),
            Uuid::new_v4().to_string(),
        )
        .fetch_one(&*DB_POOL)
        .await?;

        self.find(row.id).await
    }
}
