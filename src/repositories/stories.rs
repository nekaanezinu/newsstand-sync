use crate::{
    grpc::services::mutations::stories::CreateInput,
    models::{story::Story, user::User},
    utils::time,
};

use crate::utils::db::DB_POOL;

use super::queries::{stories, story, PaginatedList, ScopedQuery};

#[derive(Clone, Debug)]
pub struct StoriesRepository {}

impl StoriesRepository {
    pub fn new() -> Self {
        StoriesRepository {}
    }

    pub async fn all(&self, scope: stories::Scope) -> Result<Vec<Story>, sqlx::Error> {
        stories::Query::new(scope).simple().query().await
    }

    pub async fn all_paginated(
        &self,
        scope: stories::Scope,
        cursor: i32,
        limit: i32,
    ) -> Result<PaginatedList<Vec<Story>>, sqlx::Error> {
        stories::Query::new(scope)
            .paginated(cursor, limit)
            .query()
            .await
    }

    pub async fn find(&self, scope: story::Scope) -> Result<Story, sqlx::Error> {
        story::Query::new(scope).simple().query().await
    }

    pub async fn create(&self, story: &CreateInput, user: User) -> Result<Story, sqlx::Error> {
        let row = sqlx::query!(
            "INSERT INTO stories (score, title, url, favorited_at, published_at, external_id, created_at, updated_at, source_id, user_id) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING ID;",
            story.score.clone() as i32,
            story.title.clone(),
            story.url.clone(),
            time::current_primitive_datetime(),
            time::timestamp_to_primitive_datetime(&story.published_at),
            story.external_id.clone(),
            time::current_primitive_datetime(),
            time::current_primitive_datetime(),
            story.source_id.clone() as i32,
            user.id as i32,
        )
        .fetch_one(&*DB_POOL)
        .await?;

        self.find(story::Scope::User(user, row.id)).await
    }
}
