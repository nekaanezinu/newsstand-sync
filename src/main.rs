mod auth;
mod grpc;
mod models;
mod repositories;
mod utils;

#[macro_use]
extern crate lazy_static;

use std::{fs::File, io::Read};

use dotenv::dotenv;
use tonic::transport::Server;
use tonic_reflection::server::Builder as ReflectionBuilder;

use grpc::services::{
    self, sources::proto_sources::sources_crud_server::SourcesCrudServer,
    stories::proto_stories::stories_crud_server::StoriesCrudServer,
    users::users::users_crud_server::UsersCrudServer,
};
use utils::db::Db;

pub struct State {}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    dotenv().ok();

    Db::init().await.expect("init db failed");

    let addr = "0.0.0.0:50051".parse()?;
    let users_service = services::users::UsersService::new();
    let sources_service = services::sources::SourcesService::new();
    let stories_service = services::stories::StoriesService::new();

    let mut file = File::open("proto_file_descriptor.bin")?;
    let mut buf = Vec::new();
    file.read_to_end(&mut buf)?;

    let reflection_service = ReflectionBuilder::configure()
        .register_encoded_file_descriptor_set(&buf)
        .build()
        .expect("Failed to create reflection service");

    Server::builder()
        .add_service(UsersCrudServer::new(users_service))
        .add_service(SourcesCrudServer::new(sources_service))
        .add_service(StoriesCrudServer::new(stories_service))
        .add_service(reflection_service)
        .serve(addr)
        .await?;

    Ok(())
}

// #[tokio::main]
// async fn main() -> Result<(), sqlx::Error> {
// dotenv().ok();

// let db = init_db().await;
// let state = State { db };

// state.db.query().await?;

// print!("it works");

// Ok(())
// }
