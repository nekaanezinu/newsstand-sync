use tonic::{
    metadata::{Ascii, MetadataMap, MetadataValue},
    Status,
};

use crate::{models::user::User, repositories::users::UsersRepository};

pub struct Auth;

impl Auth {
    pub async fn authenticate(metadata: &MetadataMap) -> Result<User, Status> {
        let repo = UsersRepository::new();

        match metadata.get("authorization") {
            None => return Err(Status::unauthenticated("no auth header")),
            Some(auth_header) => match repo.find_by_secret(find_secret(auth_header)).await {
                Ok(user) => Ok(user),
                Err(_err) => return Err(Status::unauthenticated("invalid auth token")),
            },
        }
    }
}

fn find_secret(header: &MetadataValue<Ascii>) -> String {
    header
        .to_str()
        .unwrap()
        .to_string()
        .split("Bearer ")
        .last()
        .unwrap()
        .to_string()
}
