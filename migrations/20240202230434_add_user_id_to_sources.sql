BEGIN;

ALTER TABLE sources
ADD COLUMN external_id VARCHAR(255) NOT NULL;

CREATE UNIQUE INDEX idx_uniq_sources_user_id_external_id ON sources (user_id, external_id);

COMMIT;
